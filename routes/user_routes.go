package routes

import (
	"go-fiber-users/controllers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
)

func UserRoute(app *fiber.App) {
	// All routes related to users comes here

	app.Use(basicauth.New(basicauth.Config{
		Users: map[string]string{
			"john":   "doe",
			"admin":  "123456",
			"testgo": "772565",
		},
	}))

	api := app.Group("/api") // /api
	v1 := api.Group("/v1")   // /api/v1
	// v1.Get("/", controllers.GetHello)
	// v1.Post("/login", controllers.Login)
	// v1.Get("/user/:name", controllers.ShowParam)
	// v1.Post("/validate", controllers.AddUser)

	// v2 := api.Group("/v2")
	// v2.Get("/user/:name", controllers.ShowParam)

	v1.Get("/Users", controllers.GetUsers)
	v1.Post("/User", controllers.AddUser)
	v1.Put("/User/:id", controllers.UpdateUser)
	v1.Delete("/User/:id", controllers.RemoveUser)
	v1.Get("/User", controllers.GetUser) // User?search=id
	v1.Get("/user1", controllers.GetHello)

	app.Get("/userall", controllers.GetUsers)
	// app.Post("/login", controllers.TestLogin)
	// app.Get("/user/:name", controllers.ShowParam)

}
