package controllers

import (
	"fmt"
	"go-fiber-users/database"
	"go-fiber-users/models"

	"log"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func GetHello(c *fiber.Ctx) error {
	return c.SendString("Hello, World!")
}

func Login(c *fiber.Ctx) error {
	p := new(models.Person)

	if err := c.BodyParser(p); err != nil {
		return err
	}

	log.Println(p.Name) // john
	log.Println(p.Pass) // doe
	return c.SendString("Login Success User")
}

func ShowParam(c *fiber.Ctx) error {

	name := fmt.Sprintf("welcom %s", c.Params("name"))

	return c.SendString(name)
}

func GetUsers(c *fiber.Ctx) error {
	db := database.DBConn
	var users []models.Users

	db.Find(&users)
	return c.Status(200).JSON(users)
}

func GetUser(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var user []models.Users

	result := db.Find(&user, "id = ?", search)

	// returns found records count, equals `len(users)
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&user)
}

func AddUser(c *fiber.Ctx) error {
	db := database.DBConn
	var user models.Users

	if err := c.BodyParser(&user); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&user)
	return c.Status(201).JSON(user)
}

func UpdateUser(c *fiber.Ctx) error {
	db := database.DBConn
	var user models.Users
	id := c.Params("id")

	if err := c.BodyParser(&user); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&user)
	return c.Status(200).JSON(user)
}

func RemoveUser(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var user models.Users

	result := db.Delete(&user, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}
