package models

import (
	"time"

	"gorm.io/gorm"
)

type Model struct {
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type Person struct {
	Name string `json:"name"`
	Pass string `json:"pass"`
}

type Users struct {
	gorm.Model
	Employee_id int    `json:"employee_id"`
	Name        string `json:"name" validate:"required"`
	Lastname    string `json:"lastname" validate:"required"`
	Birthday    string `json:"birthday"`
	Age         int    `json:"age" `
	Email       string `json:"email" validate:"required,min=3,max=32"`
	Tel         string `json:"tel" validate:"required,min=3,max=20"`
}
